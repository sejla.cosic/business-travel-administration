sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.Supervisor", {

		onInit: function () {
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const collRequirements = firestore.collection("requirements").where("status", "==", "pending");

            var oRequirements = {
                requirements: []
            };

            var requirementModel = new JSONModel(oRequirements);
            this.getView().setModel(requirementModel);

            this.getRequirements(collRequirements);

            var oTextN = new sap.m.Input("name1",{editable: false});

            var oTextS = new sap.m.Input("surname1",{editable: false});

            var oTextDest = new sap.m.Input("destination1",{editable: false});

            var oTextBD = new sap.m.Input("begindate1",{editable: false});

            var oTextED = new sap.m.Input("enddate1",{editable: false});

            var that = this;

            var oDialog = new sap.m.Dialog("dialog1",{
                customHeader: new sap.m.Bar("bar",{
                    contentRight: new sap.m.Button({
                        text: "X",
                        press: function () {
                            oDialog.close();
                        }
                    })
                }),
                buttons: [new sap.m.Button("buttonSave",{
                    text: "Approve",
                    press: [function (oEvent) {
                        that.onApprove(oEvent);
                        oDialog.close();
                    }]
                }), new sap.m.Button({
                    text: "Decline",
                    press: function (oEvent) {
                        that.onDecline(oEvent);
                        oDialog.close();
                    }
                })],
                modal: true,
                content:[ oTextN, oTextS, oTextDest, oTextBD, oTextED]
            });

        },
        getRequirements: function (collRequirements) {
           /* collRequirements.get().then(
                function (collection) {
                    var requirementsModel = this.getView().getModel();
                    var requirementsData = requirementsModel.getData();
                    var requirements = collection.docs.map(function (docRequirement){
                        return docRequirement.data();
                    });
                    requirementsData.requirements = requirements;
                    this.getView().byId("requirementsList").getBinding("items").refresh();
            }.bind(this));*/


            collRequirements.onSnapshot(function (snapshot) {

                var requirementsModel = this.getView().getModel();
                var requirementsData = requirementsModel.getData();
                        
                snapshot.docChanges().forEach(function (change) {

                    var oRequirement = change.doc.data();
                    oRequirement.id = change.doc.id;
        
                    if (change.type === "added") {
                        requirementsData.requirements.push(oRequirement);
                    } 
                    else if (change.type === "modified") {
                        var index = requirementsData.requirements.map(function (requirement) {
                                return requirement.id;
                                 }).indexOf(oRequirement.id);
                        requirementsData.requirements.splice(index, 1);
                    } 
                    else if (change.type === "removed") {
                        var index = requirementsData.requirements.map(function (requirement) {
                            return requirement.id;
                             }).indexOf(oRequirement.id);
                        requirementsData.requirements.splice(index, 1);
                    }
                });
        
                this.getView().getModel().refresh(true);
                this.getView().byId("requirementsList").getBinding("items").refresh();
            }.bind(this));
        },
        onListItemPress: function (oEvent) {
            
            var oItem = oEvent.getSource().getBindingContext();
            var oDialog = sap.ui.getCore().byId("dialog1");
            oDialog.setBindingContext(oItem);

            var oTextN = sap.ui.getCore().byId("name1");
            oTextN.setValue("Name : "+oItem.getProperty("name"));

            var oTextS = sap.ui.getCore().byId("surname1");
            oTextS.setValue("Surname : "+oItem.getProperty("surename"));

            var oTextDest = sap.ui.getCore().byId("destination1");
            oTextDest.setValue("Destination : "+oItem.getProperty("destination"));

            var oTextBD = sap.ui.getCore().byId("begindate1");
            oTextBD.setValue("Begin date : "+oItem.getProperty("begindate"));

            var oTextED = sap.ui.getCore().byId("enddate1");
            oTextED.setValue("End date : "+oItem.getProperty("enddate"));

            /*var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("detail", {
                itemId: oItem.getBindingContext("request").getProperty("RequestID")
            });*/

            oDialog.open();

        },
        onApprove: function (oEvent){

            var oItem = oEvent.getSource().getBindingContext();

            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const functions = this.getOwnerComponent().getModel("firebase").getData().functions;
            const collRequirements = firestore.collection("requirements");
            
            var sendEmail = functions.httpsCallable('sendEmail');

            sendEmail({
                emailTo: oItem.getProperty("email"),
                subject: "Business Travel Request",
                message: "Dear "+oItem.getProperty("name")+", Your travel request has been approved. Best Regards, Supervisor"
            });

            sendEmail({
                emailTo: "menadzer.projekta0123@gmail.com",
                subject: "Business Travel Request",
                message: "Dear Administrator, "+oItem.getProperty("name")+"'s travel request has been approved. Best Regards, Supervisor"
            });

            this._approve(firestore,oItem,collRequirements);

            const collApproved = firestore.collection("approved");
            const collReservations = firestore.collection("reservations");
            this._addDocumentToAprovedColection(oItem,collApproved);
            this._addDocumentToReservationColection(oItem,collReservations);



        },
        _approve: function (firestore,oItem,collRequirements){
            let batch = firestore.batch();

            let updateStatus = collRequirements.doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            batch.update(updateStatus, {status: "approved"});

            // Commit the batch
            batch.commit().then(function () {
                alert("Travel approved.");
            });
        },
        _addDocumentToAprovedColection: function (oItem,collApproved){
            let data = {
                name: oItem.getProperty("name"),
                surename: oItem.getProperty("surename"),
                destination: oItem.getProperty("destination"),
                begindate: oItem.getProperty("begindate"),
                enddate: oItem.getProperty("enddate"),
                email: oItem.getProperty("email"),
                status: "pending"
            };
            let docment = collApproved.doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate")).set(data);

        },
        _addDocumentToReservationColection: function (oItem,collReservations){
            let data = {
                documentation: "",
                payments: "",
                hotel: "",
                insurance: "",
                transport: ""
            };
            let docment = collReservations.doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate")).set(data);

        },
        onDecline: function (oEvent){
            var oItem = oEvent.getSource().getBindingContext();
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const collRequirements = firestore.collection("requirements");

            let batch = firestore.batch();

            let updateStatus = collRequirements.doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            batch.update(updateStatus, {status: "declined"});

            // Commit the batch
            return batch.commit().then(function () {
                alert("Travel declined.");
            });
        }
	});
});