sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"../model/formatter"
], function(Controller, formatter) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.App", {

		formatter: formatter,

		onInit: function () {
			var invisble = new sap.ui.core.InvisibleText("invisible");
		},

		onLogin: function () {

			var email = this.byId("email").getValue();
			var password = this.byId("password").getValue();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var text = this.byId("errorText");
			
			
			var auth = this.getView().getModel("firebase").getData().auth;

			auth.signInWithEmailAndPassword(email, password).then(function(user) {
				if (email == "sef.projekta0123@gmail.com"){
					oRouter.navTo("supervisor");
				} else if (email == "menadzer.projekta0123@gmail.com"){
					oRouter.navTo("administrator");
				} else {
					sap.ui.getCore().byId("invisible").setText(email);
					oRouter.navTo("worker");
				}	
			}).catch(function(error) {
				text.setText("Please input correct email and password!");
				return;
			});
		}
	});
});