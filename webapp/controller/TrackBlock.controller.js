sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.TrackBlock", {

		onInit: function () {
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const collRequirements = firestore.collection("approved");
            var email = "";
            var invisible = sap.ui.getCore().byId("invisible").getText();
            var oRequirements = {
                filtered: []
            };
            var requirementModel = new JSONModel(oRequirements);
            this.getView().setModel(requirementModel);

            let filtered = collRequirements.where('email', '==', invisible);
            this.getRequirements(filtered);


            var oStatusH = new sap.m.ObjectStatus("hstatus");
            var oStatusT = new sap.m.ObjectStatus("tstatus");
            var oStatusD = new sap.m.ObjectStatus("dstatus");
            var oStatusI = new sap.m.ObjectStatus("istatus");
            var oStatusP = new sap.m.ObjectStatus("pstatus");

            var oTask = new sap.m.Text("task",{text: "Task"});
            var oStatus = new sap.m.Text("status",{text: "Status"});
            var oColumnTask = new sap.m.Column("columntask",{
                header: [oTask]
            });
            var oColumnStatus = new sap.m.Column("columnstatus",{
                header: [oStatus]
            });

            var oTextH = new sap.m.Input("hotel1",{
                editable: false,
                value: "Hotel reservation: "
            });

            var oTextT = new sap.m.Input("transport1",{
                editable: false,
                value: "Transport reservation: "
            });

            var oTextD = new sap.m.Input("documentation1",{
                editable: false,
                value: "Document preparation: "
            });

            var oTextI = new sap.m.Input("insurance1",{
                editable: false,
                value: "Payed insurance: "
            });

            var oTextP = new sap.m.Input("payments1",{
                editable: false,
                value: "Hotel and trasport payed: "
            });

            var oColumnH = new sap.m.ColumnListItem("columnH",{
                cells: [oTextH, oStatusH]
            });
            var oColumnT = new sap.m.ColumnListItem("columnT",{
                cells: [oTextT, oStatusT]
            });
            var oColumnD = new sap.m.ColumnListItem("columnD",{
                cells: [oTextD, oStatusD]
            });
            var oColumnI = new sap.m.ColumnListItem("columnI",{
                cells: [oTextI, oStatusI]
            });
            var oColumnP = new sap.m.ColumnListItem("columnP",{
                cells: [oTextP, oStatusP]
            });

            var oTable = new sap.m.Table("table",{
                columns: [oColumnTask, oColumnStatus],
                items: [oColumnH, oColumnT, oColumnD, oColumnI, oColumnP]
            });
            
            var oDialog = new sap.m.Dialog("dialog2",{
                customHeader: new sap.m.Bar("bar2",{
                    contentRight: new sap.m.Button({
                        text: "X",
                        press: function () {
                            oDialog.close();
                        }
                    })
                }),
                content: [oTable]
            });

        },
        getRequirements: function (collRequirements) {
            collRequirements.onSnapshot(function (snapshot) {

                var requirementsModel = this.getView().getModel();
                var requirementsData = requirementsModel.getData();
                        
                snapshot.docChanges().forEach(function (change) {

                    var oRequirement = change.doc.data();
                    oRequirement.id = change.doc.id;
        
                    if (change.type === "added") {
                        requirementsData.filtered.push(oRequirement);
                    } 
                    else if (change.type === "modified") {
                        var index = requirementsData.filtered.map(function (requirement) {
                                return requirement.id;
                                 }).indexOf(oRequirement.id);
                        requirementsData.filtered[index] = oRequirement;
                    } 
                    else if (change.type === "removed") {
                        var index = requirementsData.filtered.map(function (requirement) {
                            return requirement.id;
                             }).indexOf(oRequirement.id);
                        requirementsData.filtered.splice(index, 1);
                    }
                });
                this.getView().getModel().refresh(true);
                this.getView().byId("previewList").getBinding("items").refresh();
            }.bind(this));
        },
        showReservationStatus: function (oEvent) {
            var oItem = oEvent.getSource().getBindingContext();
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const reservation = firestore.collection("reservations").doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            reservation.get().then(function(doc) {
                if (doc.exists) {

                    var oStatusH = sap.ui.getCore().byId("hstatus");
                    if(doc.get("hotel") == ""){
                        oStatusH.setText("Missing");
                        oStatusH.setState(sap.ui.core.ValueState.None);
                    }
                    else{
                        oStatusH.setText("Done");
                        oStatusH.setState(sap.ui.core.ValueState.Success);
                    }

                    var oStatusT = sap.ui.getCore().byId("tstatus");
                    if(doc.get("transport") == ""){
                        oStatusT.setText("Missing");
                        oStatusT.setState(sap.ui.core.ValueState.None);
                    }
                    else{
                        oStatusT.setText("Done");
                        oStatusT.setState(sap.ui.core.ValueState.Success);
                    }

                    var oStatusD = sap.ui.getCore().byId("dstatus");
                    if(doc.get("documentation") == ""){
                        oStatusD.setText("Missing");
                        oStatusD.setState(sap.ui.core.ValueState.None);
                    }
                    else{
                        oStatusD.setText("Done");
                        oStatusD.setState(sap.ui.core.ValueState.Success);
                    }

                    var oStatusI = sap.ui.getCore().byId("istatus");
                    if(doc.get("insurance") == ""){
                        oStatusI.setText("Missing");
                        oStatusI.setState(sap.ui.core.ValueState.None);
                    }
                    else{
                        oStatusI.setText("Done");
                        oStatusI.setState(sap.ui.core.ValueState.Success);
                    }

                    var oStatusP = sap.ui.getCore().byId("pstatus");
                    if(doc.get("payments") == ""){
                        oStatusP.setText("Missing");
                        oStatusP.setState(sap.ui.core.ValueState.None);
                    }
                    else{
                        oStatusP.setText("Done");
                        oStatusP.setState(sap.ui.core.ValueState.Success);
                    }

                
                }
                else {
                    var oStatusH = sap.ui.getCore().byId("hstatus");
                    oStatusH.setText("Missing");
                    oStatusH.setState(sap.ui.core.ValueState.None);

                    var oStatusT = sap.ui.getCore().byId("tstatus");
                    oStatusT.setText("Missing");
                    oStatusT.setState(sap.ui.core.ValueState.None);

                    var oStatusD = sap.ui.getCore().byId("dstatus");
                    oStatusD.setText("Missing");
                    oStatusD.setState(sap.ui.core.ValueState.None);

                    var oStatusI = sap.ui.getCore().byId("istatus");
                    oStatusI.setText("Missing");
                    oStatusI.setState(sap.ui.core.ValueState.None);

                    var oStatusP = sap.ui.getCore().byId("pstatus");
                    oStatusP.setText("Missing");
                    oStatusP.setState(sap.ui.core.ValueState.None);
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });

            var oDialog = sap.ui.getCore().byId("dialog2");
            oDialog.open();
        }
    });
});