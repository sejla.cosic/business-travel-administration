sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.Administrator", {

		onInit: function () {
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const collRequirements = firestore.collection("approved").where("status", "==", "pending");

            var oRequirements = {
                approved: []
            };

            var requirementModel = new JSONModel(oRequirements);
            this.getView().setModel(requirementModel);

            this.getRequirements(collRequirements);

            var oInputH = new sap.m.Input("hinput");
            var oInputT = new sap.m.Input("tinput");
            var oInputD = new sap.m.Input("dinput");
            var oInputI = new sap.m.Input("iinput");
            var oInputP = new sap.m.Input("pinput");

            var oTask = new sap.m.Text("task1",{text: "Task"});
            var oInput = new sap.m.Text("input",{text: "Input"});
            var oColumnTask = new sap.m.Column("columntask1",{
                header: [oTask]
            });
            var oColumnInput = new sap.m.Column("columninput",{
                header: [oInput]
            });

            var oTextH = new sap.m.Input("hotel2",{
                editable: false,
                value: "Hotel reservation: "
            });

            var oTextT = new sap.m.Input("transport2",{
                editable: false,
                value: "Transport reservation: "
            });

            var oTextD = new sap.m.Input("documentation2",{
                editable: false,
                value: "Document preparation: "
            });

            var oTextI = new sap.m.Input("insurance2",{
                editable: false,
                value: "Payed insurance: "
            });

            var oTextP = new sap.m.Input("payments2",{
                editable: false,
                value: "Hotel and trasport payed: "
            });

            var oColumnH = new sap.m.ColumnListItem("columnH1",{
                cells: [oTextH, oInputH]
            });
            var oColumnT = new sap.m.ColumnListItem("columnT1",{
                cells: [oTextT, oInputT]
            });
            var oColumnD = new sap.m.ColumnListItem("columnD1",{
                cells: [oTextD, oInputD]
            });
            var oColumnI = new sap.m.ColumnListItem("columnI1",{
                cells: [oTextI, oInputI]
            });
            var oColumnP = new sap.m.ColumnListItem("columnP1",{
                cells: [oTextP, oInputP]
            });

            var oTable = new sap.m.Table("table1",{
                columns: [oColumnTask, oColumnInput],
                items: [oColumnH, oColumnT, oColumnD, oColumnI, oColumnP]
            });

            var that = this;

            var oDialog = new sap.m.Dialog("dialog3",{
                customHeader: new sap.m.Bar("bar3",{
                    contentRight: new sap.m.Button({
                        text: "X",
                        press: function () {
                            oDialog.close();
                        }
                    })
                }),
                buttons: [new sap.m.Button("buttonSave",{
                    text: "Save",
                    press: [function (oEvent) {
                        that.onSaveReservation(oEvent);
                        oDialog.close();
                    }]
                }), new sap.m.Button({
                    text: "Cancel",
                    press: function () {
                        that.onCancelReservation();
                        oDialog.close();
                    }
                })],
                content: [oTable]
            });

        },
        getRequirements: function (collRequirements) {
            collRequirements.onSnapshot(function (snapshot) {

                var requirementsModel = this.getView().getModel();
                var requirementsData = requirementsModel.getData();
                        
                snapshot.docChanges().forEach(function (change) {

                    var oRequirement = change.doc.data();
                    oRequirement.id = change.doc.id;
        
                    if (change.type === "added") {
                        requirementsData.approved.push(oRequirement);
                    } 
                    else if (change.type === "modified") {
                        var index = requirementsData.approved.map(function (requirement) {
                                return requirement.id;
                                 }).indexOf(oRequirement.id);
                        requirementsData.approved.splice(index, 1);
                    } 
                    else if (change.type === "removed") {
                        var index = requirementsData.approved.map(function (requirement) {
                            return requirement.id;
                             }).indexOf(oRequirement.id);
                        requirementsData.approved.splice(index, 1);
                    }
                });
                this.getView().getModel().refresh(true);
                this.getView().byId("approvedList").getBinding("items").refresh();
            }.bind(this));
        },
        onFinish: function (oEvent) {
            var oItem = oEvent.getSource().getBindingContext();
            const functions = this.getOwnerComponent().getModel("firebase").getData().functions;
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const reservation = firestore.collection("reservations").doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            var message = "";
            reservation.get().then(function(doc) {
                if (doc.exists) {
                    message=" Hotel reservation: "+doc.get("hotel")+" ,Transport reservation: " +doc.get("transport")+", Hotel and transport payed: "+doc.get("payments");
                } else{
                    message="";
                }
            });

            var sendEmail = functions.httpsCallable('sendEmail');

            sendEmail({
                emailTo: oItem.getProperty("email"),
                subject: "Business Travel Request",
                message: "Dear "+oItem.getProperty("name")+", The reservations and documentations for your travel request to "+oItem.getProperty("destination")+" are completed. Here is the info:"+message+" Best Regards, Administrator"
            });

            const collApproved = firestore.collection("approved");

            let batch = firestore.batch();

            let updateStatus = collApproved.doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            batch.update(updateStatus, {status: "finished"});

            // Commit the batch
            return batch.commit().then(function () {
                alert("Completed!");
            });
        },
        onUpdate: function (oEvent) {
            var oItem = oEvent.getSource().getBindingContext();
            var temp;
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const reservation = firestore.collection("reservations").doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            reservation.get().then(function(doc) {
                if (doc.exists) {
                    if((temp=doc.get("hotel")) != ""){
                        sap.ui.getCore().byId("hinput").setValue(temp);
                        temp="";
                    }
                    if((temp=doc.get("transport")) != ""){
                        sap.ui.getCore().byId("tinput").setValue(temp);
                        temp="";
                    }
                    if((temp=doc.get("documentation")) != ""){
                        sap.ui.getCore().byId("dinput").setValue(temp);
                        temp="";
                    }
                    if((temp=doc.get("insurance")) != ""){
                        sap.ui.getCore().byId("iinput").setValue(temp);
                        temp="";
                    }
                    if((temp=doc.get("payments")) != ""){
                        sap.ui.getCore().byId("pinput").setValue(temp);
                        temp="";
                    }
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });

            var oDialog = sap.ui.getCore().byId("dialog3");
            oDialog.setBindingContext(oItem);
            oDialog.open();
        },
        onSaveReservation: function (oEvent){
            var oItem = oEvent.getSource().getBindingContext();
            const firebaseModel = this.getOwnerComponent().getModel("firebase");
            const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
            const reservation = firestore.collection("reservations").doc(oItem.getProperty("email")+oItem.getProperty("destination")+oItem.getProperty("begindate"));
            
            var oInputH = sap.ui.getCore().byId("hinput");
            var oInputD = sap.ui.getCore().byId("dinput");
            var oInputI = sap.ui.getCore().byId("iinput");
            var oInputT = sap.ui.getCore().byId("tinput");
            var oInputP = sap.ui.getCore().byId("pinput");

            let batch = firestore.batch();
            if(oInputH.getValue() != ""){
                batch.update(reservation, {hotel: oInputH.getValue()});
            }
            if(oInputD.getValue() != ""){
                batch.update(reservation, {documentation: oInputD.getValue()});
            }
            if(oInputI.getValue() != ""){
                batch.update(reservation, {insurance: oInputI.getValue()});
            }
            if(oInputT.getValue() != ""){
                batch.update(reservation, {transport: oInputT.getValue()});
            }
            if(oInputP.getValue() != ""){
                batch.update(reservation, {payments: oInputP.getValue()});
            }
            // Commit the batch
            batch.commit().then(function () {
                alert("Successfully saved.");
            });

        },
        onCancelReservation: function (){
            sap.ui.getCore().byId("hinput").setValue("");
            sap.ui.getCore().byId("dinput").setValue("");
            sap.ui.getCore().byId("iinput").setValue("");
            sap.ui.getCore().byId("tinput").setValue("");
            sap.ui.getCore().byId("pinput").setValue("");
        }
    });
});