sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.RequestBlock", {

		onInit: function () {
			var email = sap.ui.getCore().byId("invisible").getText();
			this.byId("email1").setValue(email);
		},

		onApply: function () {

			var name = this.byId("name").getValue();
			var surename = this.byId("surename").getValue();
			var destination = this.byId("destination").getValue();
			var beginDate = this.byId("beginDate").getValue();
			var endDate = this.byId("endDate").getValue();
			var email = this.byId("email1").getValue();
			if ((name == "") || (surename == "") || (destination == "") || (beginDate == "") || (endDate == "")) {
				alert("Please fill in the gaps!");
			} else {
				const firebaseModel = this.getOwnerComponent().getModel("firebase");
				const firestore = this.getOwnerComponent().getModel("firebase").getData().firestore;
				const collRequirements = firestore.collection("requirements");

				let data = {
					name: name,
					surename: surename,
					destination: destination,
					begindate: beginDate,
					enddate: endDate,
					email: email,
					status: "pending"
				};

				const requirement = collRequirements.doc(email + destination + beginDate);
				requirement.get().then(function (doc) {
					if (doc.exists) {
						alert("Request already exists!")
					}
					else {
						let docment = requirement.set(data);
						alert("Request successifully sent.");
					}
				});

				this.byId("name").setValue("");
				this.byId("surename").setValue("");
				this.byId("destination").setValue("");
				this.byId("beginDate").setValue("");
				this.byId("endDate").setValue("");

			}


		}
	});
});