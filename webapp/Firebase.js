sap.ui.define([
	"sap/ui/model/json/JSONModel",
], function (JSONModel) {
    "use strict";
    
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyB3RqVVLrgc5vrBgXmBfqqdvrmo94vYsN0",
        authDomain: "my-administration-project.firebaseapp.com",
        databaseURL: "https://my-administration-project.firebaseio.com",
        projectId: "my-administration-project",
        storageBucket: "my-administration-project.appspot.com",
        messagingSenderId: "894386615947",
        appId: "1:894386615947:web:d29b1cece5bc800e"
    };
    
    return {
		initializeFirebase: function () {
			// Initialize Firebase with the Firebase-config
			firebase.initializeApp(firebaseConfig);
			
			// Create a Auth reference
			//const firestore = firebase.firestore();
			const auth = firebase.auth();
			const firestore = firebase.firestore();
			var functions = firebase.functions();

			
			// Firebase services object
			const oFirebase = {
				auth: auth,
				firestore: firestore,
				functions: functions
			};
			
			// Create a Firebase model out of the oFirebase service object which contains all required Firebase services
			var fbModel = new JSONModel(oFirebase);
			
			// Return the Firebase Model
			return fbModel;
		}
	};
});