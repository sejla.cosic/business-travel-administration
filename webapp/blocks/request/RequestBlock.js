sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var RequestBlock = BlockBase.extend("sap.ui.demo.basicTemplate.blocks.request.RequestBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "sap.ui.demo.basicTemplate.view.RequestBlock",
					type: "XML",
					async: true
				},
				Expanded: {
					viewName: "sap.ui.demo.basicTemplate.view.RequestBlock",
					type: "XML",
					async: true
				}
			}
		}
	});
	return RequestBlock;
});
