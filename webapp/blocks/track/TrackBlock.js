sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var TrackBlock = BlockBase.extend("sap.ui.demo.basicTemplate.blocks.track.TrackBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "sap.ui.demo.basicTemplate.view.TrackBlock",
					type: "XML",
					async: true
				},
				Expanded: {
					viewName: "sap.ui.demo.basicTemplate.view.TrackBlock",
					type: "XML",
					async: true
				}
			}
		}
	});
	return TrackBlock;
});