# Business Travel Administration

## Prerequisites
The **UI5 build and development tooling command line interface (UI5 CLI)** has to be installed.
For installation instructions please see [Installing the UI5 CLI](https://github.com/SAP/ui5-tooling#installing-the-ui5-cli).

## Setup
1. Clone the repository and navigate into it

    https://gitlab.com/sejla.cosic/business-travel-administration.git
    cd business-travel-administration

1. Install all dependencies

    npm install

1. Start a local server and run the application (http://localhost:8080/index.html)

    ui5 serve -o /index.html

## Login
Emails and passwords for login:

	email			|		password

sef.projekta0123@gmail.com	|	0123...0123

menadzer.projekta0123@gmail.com |	0123...0123

radnik.projekta0123@gmail.com	|	0123...0123

