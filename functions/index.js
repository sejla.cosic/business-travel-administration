'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


admin.initializeApp();

exports.sendEmail = functions.https.onCall((data) => {

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: "moja.aplikacija0123@gmail.com",
            pass: "0123...0123"
        }
    });

    const mailOptions = {
        from: "moja.aplikacija0123@gmail.com",
        to: data.emailTo,
        subject: data.subject,
        text: data.message,
        html: data.message
    };
    const getDeliveryStatus = function (error, info) {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
    };

    transporter.sendMail(mailOptions, getDeliveryStatus);
});
